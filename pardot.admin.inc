<?php

/**
 * Settings form for general pardot information.
 * 
 * Parts of this code was adapted from the Google Analytics module.
 */
function pardot_admin_form($form, &$form_state) {
  $form['pardot_a_id'] = array(
    '#title' => t('Pardot account id'),
    '#type' => 'textfield',
    '#description' => t('The value show in the pardot demo script for piAId. eg. if the script has piAId = "1001"; this field should be 1001'),
    '#default_value' => variable_get('pardot_a_id', ''),
  );
  $form['pardot_c_id'] = array(
    '#title' => t('Default Pardot campaign id'),
    '#type' => 'textfield',
    '#description' => t('The value show in the pardot demo script for piCId. eg. if the script has piCId = "1001"; this field should be 1001'),
    '#default_value' => variable_get('pardot_c_id', ''),
  );

  $form['tracking']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $options = array(
    t('Every page except the listed pages'),
    t('The listed pages only'),
  );
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

  if (module_exists('php') && $php_access) {
    $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
    $title = t('Pages or PHP code');
    $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
  }
  else {
    $title = t('Pages');
  }
  $form['tracking']['page_vis_settings']['pardot_visibility_pages'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking to specific pages'),
    '#options' => $options,
    '#default_value' => variable_get('pardot_visibility_pages', ''),
  );
  $form['tracking']['page_vis_settings']['pardot_pages'] = array(
    '#type' => 'textarea',
    '#title' => $title,
    '#title_display' => 'invisible',
    '#default_value' => variable_get('pardot_pages', PARDOT_PAGES),
    '#description' => $description,
    '#rows' => 10,
  );


  return system_settings_form($form);
}

